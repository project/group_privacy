<?php

namespace Drupal\group_privacy;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface;

/**
 * Service description.
 */
class QueryAlterHelper implements QueryAlterHelperInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The group_relation_type.manager service.
   *
   * @var \Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface
   */
  protected GroupRelationTypeManagerInterface $pluginManager;

  /**
   * The current database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The query cacheable metadata.
   *
   * @var Drupal\Core\Cache\CacheableMetadata\CacheableMetadata
   */
  protected $cacheableMetadata;

  /**
   * Constructs a QueryAlterHelper object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    GroupRelationTypeManagerInterface $manager,
    Connection $database
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $manager;
    $this->database = $database;
    $this->cacheableMetadata = new CacheableMetadata();
  }

  /**
   * {@inheritDoc}
   */
  public function alter(AlterableInterface $query, string $entity_type_id) {
    $operation = $query->getMetaData('op') ?: 'view';
    // START PART FROM Drupal\group\QueryAccess\EntityQueryAlter::doAlter().
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    if (!$storage instanceof SqlContentEntityStorage) {
      return;
    }

    // Find all of the group relations that define access.
    $plugin_ids = $this->pluginManager->getPluginIdsByEntityTypeAccess($entity_type_id);
    if (empty($plugin_ids)) {
      return;
    }

    $group_relationship_plugin_id = $this->entityTypeManager->hasDefinition('group_relationship')
      ? 'group_relationship' : 'group_content';
    // If any new relationship entity is added using any of the retrieved
    // plugins, it might change access.
    $cache_tags = [];
    foreach ($plugin_ids as $plugin_id) {
      $cache_tags[] = "$group_relationship_plugin_id:plugin:$plugin_id";
    }
    $this->cacheableMetadata->addCacheTags($cache_tags);

    // If there are no relationships using the plugins, there's no point in going
    // any further. The cache tags above will invalidate our result if new group
    // content is created using the plugins that define access. Retrieve the
    // plugin IDs in use to optimize a loop further below.
    $group_relationship_data_table = $this->entityTypeManager->getDefinition($group_relationship_plugin_id)->getDataTable();
    $plugin_ids_in_use = $this->database
      ->select($group_relationship_data_table, 'gc')
      ->fields('gc', ['plugin_id'])
      ->condition('plugin_id', $plugin_ids, 'IN')
      ->distinct()
      ->execute()
      ->fetchCol();

    if (empty($plugin_ids_in_use)) {
      return;
    }

    // Check if any of the plugins actually support the operation. If not, we
    // can simply bail out here to play nice with other modules that do support
    // the provided operation.
    $operation_is_supported = FALSE;
    foreach ($plugin_ids_in_use as $plugin_id) {
      if ($this->pluginManager->getAccessControlHandler($plugin_id)->supportsOperation($operation, 'entity')) {
        $operation_is_supported = TRUE;
        break;
      }
    }

    if (!$operation_is_supported) {
      return;
    }
    // END PART FROM Drupal\group\QueryAccess\EntityQueryAlter::doAlter().
    $entity_type = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->getEntityType();
    $id_key = $entity_type->getKey('id');

    // Loop through our tables and pick up the base table, membership
    // table and (if applicable) the group content join table, which may
    // already exist for some group query alters.
    $join_alias_relationship = '';
    foreach ($query->getTables() as $alias => $table_info) {
      if ($table_info['join type'] === NULL) {
        $base_table = $alias;
      }
      elseif (
        $table_info['table'] === $group_relationship_data_table
        && str_contains($table_info['condition'], ".plugin_id='group_membership' AND gcfd_2.entity_id=:account_id")
      ) {
        $join_alias_membership = $alias;
      }
      elseif (
        $table_info['table'] === $group_relationship_data_table
        && $table_info['condition'] === "$base_table.$id_key=$alias.entity_id AND $alias.plugin_id IN (:plugin_ids_in_use[])"
      ) {
        $join_alias_relationship = $alias;
      }
    }

    // If we somehow don't have a base or membership table abort.
    if (!$base_table || !$join_alias_membership) {
      return;
    }

    // If the join to the relationship table doesn't already exist (e.g.
    // for group content queries) and we're not querying groups
    // (in which case we don't need to join groups to itself) add it, and
    // also add a join to the actual groups table.
    if (!$join_alias_relationship && $entity_type_id !== 'group') {
      $join_alias_relationship = $query->leftJoin(
        $group_relationship_data_table,
        'gcfd',
        "$base_table.$id_key=%alias.entity_id AND %alias.plugin_id IN (:plugin_ids_in_use[])",
        [':plugin_ids_in_use[]' => $plugin_ids_in_use]
      );
    }

    $join_alias_group = $query->leftJoin(
      $this->entityTypeManager->getStorage('group')->getDataTable(),
      'gfd',
      "$join_alias_relationship.gid=%alias.id"
    );

    // Find the outsider-scoped subconditions and restrict them to
    // non-private groups only. We do this by finding condition groups
    // that add a condition for membership being NULL (i.e. the user is
    // a group outsider).
    foreach ($query->conditions() as &$condition_info) {
      if (is_array($condition_info)
        && isset($condition_info['field'])
        && $condition_info['field'] instanceof ConditionInterface
        && !empty($condition_info['field']->conditions())) {

        $this->privatizeOutsiderCondition(
          $condition_info['field'],
          $join_alias_group,
          $join_alias_membership
        );
      }
    }
  }

  protected function privatizeOutsiderCondition(
    Condition $condition,
    $join_alias_group,
    $join_alias_membership
  ) {
    foreach ($condition->conditions() as &$subcondition) {
      if (
        is_array($subcondition)
        && is_object($subcondition['field'])
        && $this->privatizeOutsiderCondition($subcondition['field'], $join_alias_group, $join_alias_membership)
      ) {
        $subcondition['field']->condition("$join_alias_group.is_private", TRUE, '!=');
        return FALSE;
      }
      elseif (
        is_array($subcondition)
        && $subcondition['field'] === "$join_alias_membership.entity_id"
        && $subcondition['operator'] === 'IS NULL'
        && $subcondition['value'] === NULL
      ) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
