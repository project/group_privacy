<?php

namespace Drupal\group_privacy\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupPermissionChecker as GroupPermissionCheckerBase;
use Drupal\group\Entity\GroupInterface;

/**
 * Calculates group permissions for an account.
 */
class GroupPermissionChecker extends GroupPermissionCheckerBase {

  /**
   * {@inheritdoc}
   */
  public function hasPermissionInGroup(
    $permission,
    AccountInterface $account,
    GroupInterface $group
  ) {
    // Automatically deny permission for all operations to group outsiders
    if (
      !$account->hasPermission('bypass group privacy')
      && $group->is_private->value
      && !$this->groupMembershipLoader->load($group, $account)
    ) {
      return FALSE;
    }
    // Otherwise fall back to normal group permission handling.
    else {
      return parent::hasPermissionInGroup($permission, $account, $group);
    }
  }

}
