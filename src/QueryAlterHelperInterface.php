<?php

namespace Drupal\group_privacy;

use Drupal\Core\Database\Query\AlterableInterface;

/**
 * Service description.
 */
interface QueryAlterHelperInterface {

  /**
   * Alter the query.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The query to alter.
   * @param string $entity_type_id
   *   The entity type ID.
   */
  public function alter(AlterableInterface $query, string $entity_type_id);

}
