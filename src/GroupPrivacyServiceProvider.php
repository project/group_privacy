<?php

namespace Drupal\group_privacy;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Drupal\group_privacy\Access\GroupPermissionChecker;

/**
 * Service provider for the echidnet_workflow module.
 *
 * This is used to override group's own overridden content moderation
 * functionality to allow detecting the group from the groups field.
 */
class GroupPrivacyServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');

    // Decorate the state transition validation service.
    $state_transition_definition = new Definition(GroupPermissionChecker::class, [
      new Reference('group_permission.calculator'),
      new Reference('group.membership_loader'),
    ]);
    $state_transition_definition->setPublic(TRUE);
    $state_transition_definition->setDecoratedService('group_permission.checker');
    $container->setDefinition('group_privacy.checker', $state_transition_definition);
  }

}
